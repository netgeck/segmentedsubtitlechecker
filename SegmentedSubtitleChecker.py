#!/usr/bin/python
import io
import pathlib
import sys
import xml.etree.ElementTree as ET
import natsort
from pymp4.parser import Box


if len(sys.argv) != 2:
    print("Usage: " + sys.argv[0] + " <directory>")
    exit(-1)

path = pathlib.Path(sys.argv[1])


def process_file(currentFile):
    print(currentFile)
    with open(currentFile, 'rb') as fd:
        fd.seek(0, io.SEEK_END)
        eof = fd.tell()
        fd.seek(0)

        base_media_decode_time = 0
        ttml_subtitle_string = ""

        while fd.tell() < eof:
            try:
                box = Box.parse_stream(fd)
                content = dict(box.__getstate__())
                ctype = content['type'].decode('utf8')
                if ctype == "moof":
                    moof_box = content
                    moof_children = moof_box['children']
                    for box in moof_children:
                        if box['type'].decode('utf8') == 'traf':
                            moof_traf_children = box['children']
                            for box in moof_traf_children:
                                if box['type'].decode('utf8') == 'tfdt':
                                    tfdt = box
                                    base_media_decode_time = tfdt['baseMediaDecodeTime']
                                    break
                if ctype == 'mdat':
                    ttml_subtitle_string = content['data'].decode('utf8')

            except Exception as e:
                print(e)
                raise

        print("baseMediaDecodeTime = " + str(base_media_decode_time))

        if ttml_subtitle_string:
            xml_root = ET.fromstring(ttml_subtitle_string)

            xml_body = xml_root.find('{http://www.w3.org/ns/ttml}body')
            for div in xml_body.findall('{http://www.w3.org/ns/ttml}div'):
                for p in div.findall('{http://www.w3.org/ns/ttml}p'):
                    print(p.attrib)
                    for span in p.findall('{http://www.w3.org/ns/ttml}span'):
                        print("\t" + span.text)


if path.is_file():
    process_file(path)
elif path.is_dir():
    names_in_dir = []
    for file_name in path.iterdir():
        names_in_dir.append(file_name.name)
    sorted_names = natsort.natsorted(names_in_dir)
    print(sorted_names)

    for name in sorted_names:
        process_file(path / name)
